var path = require('path');
var webpack = require('webpack');
var autoprefixer = require('autoprefixer');
var ExtractTextPlugin = require("extract-text-webpack-plugin");

var externals = {}
var plugins = [
  new webpack.NoEmitOnErrorsPlugin(),
  new ExtractTextPlugin({filename: 'style.css', allChunks: true})
];

if (process.env.NODE_ENV == 'production') {
  plugins.push(new webpack.DefinePlugin({
    'process.env': {
      'NODE_ENV': JSON.stringify('production')
    }
  }));
}

module.exports = {
  devtool: 'source-map',
  entry: [
    './src/client',
  ],
  module: {
    rules: [
      {
        test: /\.cjsx$/,
        use: ['coffee-loader', 'cjsx-loader']
      },
      {
        test: /\.coffee$/,
        use: ['coffee-loader']
      },
      {
        test: /\.png$/,
        use: ['url-loader']
      },
      {
        test: /\.scss$/,
        use: ExtractTextPlugin.extract(
          {
            fallback: 'style-loader',
            use: ['css-loader',
            {
              loader: 'postcss-loader',
              options: {
                plugins: function () {
                  return [
                    require('autoprefixer')
                  ]
                }
              }
            },
            'resolve-url-loader',
            {
              loader: 'sass-loader',
              options: { sourceMap: true }
            }]
          }
        )
      }
    ]
  },
  plugins: plugins,
  output: {
      path: path.join(__dirname, 'public', 'dist'),
      filename: "bundle.js",
      publicPath: '/dist/'
  },
  resolve: {
    extensions: ['.js', '.cjsx', '.coffee']
  }
};
