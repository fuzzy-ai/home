Hacking fuzzy.ai
================

If you're working on the fuzzy.ai Web front-end, you can set up a dev
environment by following these steps.

### System Setup

These are the steps you should only have to do once on your machine to get all of the tools in place.

1. Clone the fuzzyio/home project from Github. The Git URL is `git@github.com:fuzzy-ai/home.git` . It is a private repository so you need to have an SSH key set up with Github.

1. Install Node.js. Follow the instructions at http://nodejs.org/

   You should be able to run `node --version` and `npm --version` from the command line.

1. Install Coffeescript. From the command line, run:

        npm install -g coffee-script

   You should be able to run `coffee --version` and `cake` from the
   command line.

1. Install Docker. See https://docs.docker.com/engine/installation/
   for details.

   You should be able to run `docker -v` from the command line.

1. Install Docker Compose. See https://docs.docker.com/compose/install/ for details.

   You should be able to run `docker-compose -v` from the command line.

1. Install and authenticate the Google Cloud SDK. See: https://cloud.google.com/sdk/

   Once the SDK is installed, you should be able to run the following to authenticate (using your fuzzy account):

        gcloud auth login

1. Authenticate docker to use the gcr.io repository:

        docker login -u oauth2accesstoken -p "$(gcloud auth print-access-token)" https://gcr.io

1. *OSX Only* Create / run a docker machine:

        docker-machine create --driver=virtualbox fuzzy

### Working Steps

The following steps you'll need whenever you're working on the code. Remember to `git pull` so that your code is up to date!

1. *OSX Only* Ensure your docker machine is running:

        docker-machine start fuzzy

1. *OSX Only* Set the docker machine environment variables.

        eval $(docker-machine env fuzzy)

1. Make sure you have the latest images. From the command line, run:

        docker-compose pull

1. Start the microservices. From the command line, run:

        docker-compose up -d

1. Install all the dependencies. From the command line, run:

        npm install

1. Launch the development server. In the main directory of the home project, run:

        cake dev

1. You can now view the site at [http://localhost:8000](http://localhost:8000)

1. Edit what you want. Any scss or cjsx file changes should live reload. :rocket:

### Running with Docker

If you want to run locally with the Docker container, use this command:

  docker-compose -f docker-compose.yml -f home.yml up -d

That will run the microservices, build the home image, and run the home image.

### Developer tools

There are a few browser extensions that make working with react/redux a bit easier:

* https://github.com/facebook/react-devtools
* https://github.com/gaearon/redux-devtools

### Coding Standards

Install and run coffeelint

Install via `npm install -g coffeelint`

Run via `coffeelint --ext cjsx,coffee src`
