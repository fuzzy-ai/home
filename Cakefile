fs = require 'fs'
url = require 'url'

{print} = require 'util'
{spawn} = require 'child_process'

glob = require 'glob'
_ = require 'lodash'

DOCKER = "fuzzyai/home"

cmd = (str, env, callback) ->
  if _.isFunction(env)
    callback = env
    env = null
  env = _.defaults(env, process.env)
  parts = str.split(' ')
  main = parts[0]
  rest = parts.slice(1)
  proc = spawn main, rest, {env: env}
  proc.stderr.on 'data', (data) ->
    process.stderr.write data.toString()
  proc.stdout.on 'data', (data) ->
    print data.toString()
  proc.on 'exit', (code) ->
    if code is 0
      callback?()
    else
      process.exit(code)

build = (callback) ->
  env =
    NODE_ENV: 'production'
  cmd './node_modules/.bin/cjsx -c -o lib src', ->
    cmd './node_modules/.bin/webpack -p --bail', env, callback

buildDocker = (callback) ->
  cmd "docker build -t #{DOCKER} .", callback

buildTest = (callback) ->
  cmd "./node_modules/.bin/cjsx -c test", callback

task "build", "Build lib/ from src/", ->
  build()

task 'dev', 'Launch server in development mode', ->
  if process.env.DOCKER_HOST
    host = url.parse(process.env.DOCKER_HOST).hostname
  else
    host = 'localhost'
  env =
    NODE_ENV: 'development'
    WEB_PORT: 8000
    WEB_API_SERVER: "http://#{host}:8001"
    WEB_STATS_SERVER: "http://#{host}:8002"
    WEB_STATS_KEY: 'boyle-punic-biceps-kalmia-hoop'
    WEB_AUTH_SERVER: "http://#{host}:8003"
    WEB_AUTH_KEY: 'aDnwaMB2u3k'
    WEB_PLAN_SERVER: "http://#{host}:8004"
    WEB_PLAN_KEY: 'koozaiX7ruh'
    WEB_PAYMENTS_SERVER: "http://#{host}:8005"
    WEB_PAYMENTS_KEY: 'aph8eebiS1d'
    WEB_STRIPE_PUBLIC_KEY: process.env.STRIPE_PUBLIC_KEY
    WEB_TEMPLATES_SERVER: "http://#{host}:8006"
    WEB_TEMPLATES_KEY: "ooToog9Hahv"
    DRIVER: 'redis'
    PARAMS: '{"host": "' + host + '", "database": 0}'
  cmd "npm run dev", env

task 'watch', 'Watch src/ for changes', ->
  coffee = spawn 'coffee', ['-w', '-c', '-o', 'lib', 'src']
  coffee.stderr.on 'data', (data) ->
    process.stderr.write data.toString()
  coffee.stdout.on 'data', (data) ->
    print data.toString()

task 'clean', 'Clean up extra files', ->
  patterns = [
    "lib/*.js"
    "lib/actions/*.js"
    "lib/components/*.js"
    "lib/reducers/*.js"
    "test/*.js"
    "*~"
    "lib/*~"
    "src/*~"
    "test/*~"
  ]
  for pattern in patterns
    glob pattern, (err, files) ->
      for file in files
        fs.unlinkSync file

task "test", "Run tests", ->
  buildTest ->
    # Wasn't globbing so doing it manually
    glob "test/*-test.js", (err, files) ->
      cmd "./node_modules/.bin/perjury #{files.join(' ')}"

task 'docker', 'Build docker image', ->
  invoke 'clean'
  buildDocker()

task "push", "Push docker image", ->
  cmd "docker push #{DOCKER}"
