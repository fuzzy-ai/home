require 'babel-polyfill'
moment = require 'moment'
React = require 'react'
{ render } = require 'react-dom'

{ Provider } = require 'react-redux'
{ Router, browserHistory, applyRouterMiddleware } = require 'react-router'
{ syncHistoryWithStore } = require 'react-router-redux'

{ useScroll } = require 'react-router-scroll'

configureRoutes = require './components/routes'
configureStore = require './store'

initialState = window.__INITIAL_STATE__

store = configureStore(browserHistory, initialState)
history = syncHistoryWithStore(browserHistory, store)
routes = configureRoutes(store)

# wire in analytics
history.listen (location) ->
  state = store.getState()
  user = state.user.account
  if user
    window?.Intercom? 'update',
      email: user.email
      user_id: user.id
      created_at: moment(user.createdAt).unix()
      plan: user.plan
  else
    window?.Intercom? 'update'
  window?.ga? 'send', 'pageview'
  window?.twq? 'track','PageView'
  window?.fbq? 'track', 'PageView'

require '../public/assets/scss/style.scss'

render(
  <Provider store={store}>
    <Router
      history={history}
      routes={routes}
      render={applyRouterMiddleware(useScroll())}
    />
  </Provider>
, document.getElementById('app'))
