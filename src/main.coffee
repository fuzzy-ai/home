# main.coffee
# Copyright 2014 9165584 Canada Corporation <legal@fuzzy.ai>
# All rights reserved.

require 'dns-notfound-what'

WebServer = require './webserver'

envBool = (str) ->
  str?.toLowerCase() in ["true", "yes", "1", "on"]

# coffeelint: disable=max_line_length
config =
  port: process.env['WEB_PORT']
  hostname: process.env['WEB_HOSTNAME']
  address: process.env['WEB_ADDRESS']
  key: process.env['WEB_KEY']
  cert: process.env['WEB_CERT']
  authServer: process.env['WEB_AUTH_SERVER']
  authKey: process.env['WEB_AUTH_KEY']
  sessionSecret: process.env['WEB_SESSION_SECRET']
  apiServer: process.env['WEB_API_SERVER']
  apiKey: process.env['WEB_API_KEY']
  logLevel: process.env['WEB_LOG_LEVEL']
  statsServer: process.env['WEB_STATS_SERVER']
  statsKey: process.env['WEB_STATS_KEY']
  planServer: process.env['WEB_PLAN_SERVER']
  planKey: process.env['WEB_PLAN_KEY']
  paymentsServer: process.env['WEB_PAYMENTS_SERVER']
  paymentsKey: process.env['WEB_PAYMENTS_KEY']
  templatesServer: process.env['WEB_TEMPLATES_SERVER']
  templatesKey: process.env['WEB_TEMPLATES_KEY']
  caCert: process.env['WEB_CA_CERT']
  driver: process.env['DRIVER']
  params: if process.env['PARAMS']? then JSON.parse(process.env['PARAMS']) else {}
  cleanup: if process.env['CLEANUP']? then parseInt(process.env['CLEANUP'], 10) else null
  slackHook: process.env['SLACK_HOOK']
  apiClientCacheSize: if process.env.API_CLIENT_CACHE_SIZE? then parseInt(process.env.API_CLIENT_CACHE_SIZE) else undefined
  webClientTimeout: if process.env['WEB_CLIENT_TIMEOUT']? then parseInt(process.env['WEB_CLIENT_TIMEOUT'], 10) else Infinity
  redirectToHttps: if process.env['REDIRECT_TO_HTTPS']? then envBool(process.env['REDIRECT_TO_HTTPS']) else false
  sessionMaxAge: if process.env['WEB_SESSION_MAX_AGE']? then parseInt(process.env['WEB_SESSION_MAX_AGE'], 10) else undefined
  sessionSecure: if process.env['WEB_SESSION_SECURE']? then envBool(process.env['WEB_SESSION_SECURE']) else false

server = new WebServer(config)

# No errors on too many listeners

process.setMaxListeners 0

shutdown = ->
  console.log "Shutting down..."
  server.stop (err) ->
    if (err)
      console.error err
      process.exit -1
    else
      console.log "Done."
      process.exit 0

process.on 'SIGTERM', shutdown
process.on 'SIGINT', shutdown

# XXX: send a message to Slack for uncaught exceptions

process.on 'uncaughtException', (err) ->
  console.log "******************** UNCAUGHT EXCEPTION ********************"
  console.error err
  if err.stack
    console.dir err.stack.split("\n")
  process.exit 127

server.start (err) ->
  if err
    console.error(err)
  else
    console.log("Server listening on #{server.config.port} for #{server.config.address or server.config.hostname}")
