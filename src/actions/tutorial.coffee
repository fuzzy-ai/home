# src/actions/tutorial.coffee
# Copyright 2016 9165584 Canada Corporation <legal@fuzzy.ai>
# All rights reserved.

require 'es6-promise'
_ = require 'lodash'
fetch = require 'isomorphic-fetch'

{ checkResponse } = require '.'

setTutorial = ->
  type: 'SET_TUTORIAL'

setTutorialSuccess = (data) ->
  type: 'SET_TUTORIAL_SUCCESS'
  agentID: data.agentID
  step: data.step
  completed: data.completed

setTutorialError = (data, err) ->
  type: 'SET_TUTORIAL_ERROR'
  error: err

exports.updateTutorial = (data) ->
  (dispatch) ->
    dispatch setTutorial()
    fetch("/data/tutorial",
      method: 'post'
      credentials: 'same-origin'
      headers:
        'Accept': 'application/json'
        'Content-Type': 'application/json'
        'X-CSRF-Token': window.CSRF_TOKEN
      body: JSON.stringify data)
    .then (response) -> checkResponse(response, dispatch)
    .then (json) ->
      dispatch setTutorialSuccess(json.user.tutorial)
    .catch (err) ->
      dispatch setTutorialError(data, err)
