_ = require 'lodash'
React = require 'react'
Link = require 'react-router/lib/Link'
{ connect } = require 'react-redux'
{ push } = require 'react-router-redux'

{ fetchCustomer,
  createCustomer,
  updateCustomer,
  customerError } = require '../actions/payments'
{ fetchPlans } = require '../actions/plans'
CreditCard = require './credit-card'
ErrorBar = require './error-bar'

Upgrade = React.createClass
  displayName: "Upgrade"

  getInitialState: ->
    newCard: false

  componentDidMount: ->
    { dispatch } = @props
    dispatch(fetchPlans())
    dispatch(fetchCustomer())

  toggleNewCard: (e) ->
    e.preventDefault()
    @setState newCard: not @state.newCard

  handleSubmit: (target) ->
    { dispatch, selectedPlan, customer, user } = @props
    Stripe.card.createToken target, (status, response) ->
      if response.error
        dispatch(customerError(response.error))
      else
        props =
          source: response.id
          plan: selectedPlan
        if customer
          dispatch(updateCustomer(props))
        else
          props.coupon = user?.couponCode
          dispatch(createCustomer(props))

  updatePlan: (e) ->
    e.preventDefault()

    { dispatch, selectedPlan } = @props
    dispatch(updateCustomer({plan: selectedPlan}))
    dispatch(push('/'))

  # coffeelint: disable=max_line_length
  render: ->
    { newCard } = @state
    { plans, customer, selectedPlan, inProgress, plansError, paymentsError } = @props
    plan = _.find plans, {stripeId: selectedPlan}
    card = customer?.sources?.data[0]

    <div className="row">
      {if plansError?
        <ErrorBar err={plansError} />
      }
      {if paymentsError?
        <ErrorBar err={paymentsError} />
      }
      <div className="main">
      { if plan
        <div className="mbl unit size1of2 upgrade-option">
          <h2>Upgrade to {plan.name}</h2>
          <ul className="compressed-list">
            <li className="compressed-list__item">
              <span className="fwb">${plan.price / 100}</span>
              <span className="fwb"> /{plan.period}</span>
            </li>
            <li className="compressed-list__item"><strong>{plan.apiLimit} </strong>Monthly API calls</li>
            <li className="compressed-list__item"><strong>Chat & email </strong>support</li>
          </ul>
        </div>

      }
      {if card
        <div className="unit size1of2">
          <p>You currently have a credit card on file ending in <span className="fwb">{card.last4}</span></p>
          {if not newCard
            <a onClick={@updatePlan} className="btn btn__regular mr-40">Update Plan</a>
          }
          <a onClick={@toggleNewCard} className=" fwb upper small-text">Click here to change</a>

        </div>
      }
      {if not card or newCard
        <div className="unit size1of2">
          <CreditCard submitHandler={@handleSubmit} inProgress={inProgress}/>
        </div>
      }

      </div>
    </div>

mapStateToProps = (state, ownProps) ->
  selectedPlan: ownProps.location.query.plan
  plans: state.plans.items
  plansError: state.plans.error
  inProgress: state.payments.inProgress
  paymentsError: state.payments.error
  customer: state.payments.customer
  user: state.user.account


module.exports = connect(mapStateToProps)(Upgrade)
