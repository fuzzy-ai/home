# agents-edit-advanced.cjsx
# Copyright 2016 Fuzzy.ai <legal@fuzzy.ai>
# All rights reserved.

React = require 'react'
_ = require 'lodash'
{ connect } = require 'react-redux'
{ DeleteCross } = require './icons'

Dropdown = require './dropdown'
FuzzyInput = require './fuzzy-input'

Input = React.createClass
  displayName: "Input"

  getInitialState: ->
    type: @props.type

  _getCount: ->
    type = @state.type
    switch type
      when "slope up", "slope down"
        2
      when "triangle"
        3
      when "trapezoid"
        4

  _handleTypeChange: (e) ->
    e.preventDefault()
    @setState type: @refs.type.value

  _handleChange: (e) ->
    e.preventDefault()
    name = @refs.name.value
    count = @_getCount()
    set = []

    for i in [0..count - 1]
      set[i] = _.toNumber @refs["set-#{i}"].value

    @props.changeHandler(name, set, @props.name)

  # coffeelint: disable=max_line_length
  render: ->
    { name, type, set } = @props
    count = @_getCount()
    <div className="sets">
      <div>
        <h3 className="app-heading upper fwb">name</h3>
        <input type="text" defaultValue={@props.name} ref="name" onBlur={@_handleChange} />
      </div>
      <div>
      <h3 className="app-heading upper fwb">type</h3>
        <div className="select">
          <select value={@state.type} ref="type" onChange={@_handleTypeChange}>
            <option value="slope down">slope down</option>
            <option value="slope up">slope up</option>
            <option value="triangle">triangle</option>
            <option value="trapezoid">trapezoid</option>
          </select>
        </div>
      </div>
      <div>
      <h3 className="app-heading upper fwb">Value</h3>
        {for i in [0..count - 1]
          <input key={i} type="text" value={set[i] or 0} ref="set-#{i}" onChange={(e) -> set[i] = @refs["set-#{i}"]} onBlur={@_handleChange}/>
        }
      </div>
    </div>

AgentsEditAdvanced = React.createClass
  displayName: 'Agents Edit Advanced'

  getInitialState: ->
    sets: @props.input


  _determineType: (sets, set) ->
    all = _.flatten _.values sets
    min = _.min all
    max = _.max all

    switch set.length
      when 2
        if set[0] == min
          "slope down"
        else
          "slope up"
      when 3
        "triangle"
      when 4
        "trapezoid"

  _handleChange: (name, set, oldName) ->
    sets = @state.sets
    sets[name] = set
    if oldName != name
      delete sets[oldName]
    @setState sets: sets
    @props.handleSets(sets)





  # coffeelint: disable=max_line_length
  render: ->
    { sets, showAdvanced, toggleAdvancedEdit} = @state
    handleChange = @_handleChange




    <div>
      <h2 className="app-heading">Advanced Range Editor</h2>
      <FuzzyInput width={800} height={240} padding={30} sets={sets} changeHandler={handleChange}/>
      <div>
        {for setName, set of sets
          type = @_determineType(sets, set)
          <Input key={setName} name={setName} set={set} type={type} changeHandler={handleChange}/>
        }
      </div>
    </div>


module.exports = AgentsEditAdvanced
