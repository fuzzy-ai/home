_ = require 'lodash'
React = require 'react'
Link = require 'react-router/lib/Link'
{ connect } = require 'react-redux'
{ push } = require 'react-router-redux'


{ storeAgent } = require '../actions/agent'
RangeInput = require './range-input'
ErrorBar = require './error-bar'
PerformanceMetric = require './performance-metric'


AgentsSettings = React.createClass
  displayName: 'AgentSettings'

  getInitialState: ->
    showRange: false
    range: []
    showPerformance: false
    performance: {}

  componentDidMount: ->
    { agent } = @props
    @_determineRange(agent)
    @_determinePerformance(agent)

  componentWillReceiveProps: (nextProps) ->
    { agent } = nextProps
    @_determineRange(agent)
    @_determinePerformance(agent)

  toggleRange: (e) ->
    e.preventDefault()
    @setState showRange: true

  handleRange: (range) ->
    @setState range: range

  handleSubmit: (e) ->
    e.preventDefault()
    { agent, dispatch } = @props
    newAgent = _.cloneDeep(agent)
    newAgent.name = @refs.name.value
    newAgent.description = @refs.description.value

    if @state.range.length
      output = _.keys(newAgent.outputs)[0]
      newAgent.outputs[output] = @state.range

    newAgent.performance = @state.performance

    dispatch(storeAgent(newAgent, agent))
    dispatch(push("/agents/#{agent.id}"))

  _determineRange: (agent) ->
    if not @state.range.length
      values = _.flatten _.values _.values(agent?.outputs)[0]
      if values.length
        range = [_.min(values), _.max(values)]
        @setState range: range

  _determinePerformance: (agent) ->
    if not _.keys(@state.performance).length and agent?.performance
      @setState performance: agent.performance

  toggleShowAddMetric: ->
    @setState showAddMetric: not @state.showAddMetric

  handleAddMetric: (e) ->
    e.preventDefault()

    performance = @state.performance
    name = @refs.metricName.value
    value = @refs.metricValue.value

    performance[name] = value
    @setState
      performance: performance
      showAddMetric: false
    @refs.metricName.value = ''

  deleteMetric: (name) ->
    performance = @state.performance
    delete performance[name]
    @setState performance: performance

  # coffeelint: disable=max_line_length
  render: ->
    { agent, error } = @props
    { showRange } = @state

    <div className="agent__settings">
      {if error
        <ErrorBar err={error} />
      }
      <form onSubmit={@handleSubmit} role="form" className="agent__settings-form">
        {if agent?.id
          <div className="form-group input-wrap">
            <label htmlFor="" className="upper">Name</label>
            <input ref="name" className="form-control" defaultValue={agent?.name} />
          </div>
        }
        {if agent?.id
          <div className="form-group input-wrap">
            <label htmlFor="" className="upper">Description</label>
            <textarea ref="description" className="form-control" placeholder="Agent Description" defaultValue={agent?.description}></textarea>
          </div>
        }
        {if @state.range.length
          <section className="secondary-settings-section m-40-0-0 pad-40-0">
            <div className="form-group input-wrap">
              <label htmlFor="" className="upper h3">Output Range</label>
              <RangeInput handleRange={@handleRange} fromValue={@state.range[0]} toValue={@state.range[1]} />
            </div>
          </section>
        }
        {if agent?.id
          <section className="secondary-settings-section  pad-40-0">
            <div className="form-group input-wrap">
              <header>
                <h3 className="upper">Performance Metrics</h3>
              </header>
              { for name, value of @state.performance
                <PerformanceMetric key={name} name={name} value={value} deleteHandler={@deleteMetric}/>
              }
              <a onClick={@toggleShowAddMetric} className="line sup-heading  upper fwb mbr">Add Performance Metrics +</a>
              {if @state.showAddMetric

                <form onSubmit={@handleAddMetric}>
                  <label htmlFor="new-metric-name">Name</label>
                  <input type="text" id="new-metric-name" ref="metricName" placeholder="Metric Name" />

                  <select ref="metricValue" className="metric-select">
                    <option>maximize</option>
                    <option>minimize</option>
                  </select>
                  <button className="btn btn-clear" onClick={@handleAddMetric}>Add</button>
                </form>
              }
            </div>
          </section>
        }
        <div className="save-cancel-action-wrap">
          <button className="btn btn__regular" type="submit">Save</button>
          <Link to="/agents/#{agent?.id}" className="btn__regular btn--cancel">Cancel</Link>
        </div>
      </form>
      <div className="agent-edit__footer">
        <Link to="/agents/#{agent?.id}/delete" className="trash">
          <svg className="delete-icon" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 32 32">
            <use xlinkHref="/assets/sprite.svg#icon-trash"></use>
          </svg>
        </Link>
      </div>
    </div>

mapStateToProps = (state) ->
  agent: state.agent.current
  error: state.agent.error

module.exports = connect(mapStateToProps)(AgentsSettings)
