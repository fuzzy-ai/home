React = require 'react'
{ Link } = require 'react-router'

DocsSidebarNav = require './docs-sidebar-nav'
DocsIndex = require './docs-index'

Docs = React.createClass
  displayName: "Docs"


  render: ->
    <div className="row">
      <div className="docs-wrapper page">
        <DocsSidebarNav />
        <div className="docs-content">
          { @props.children}

        </div>
      </div>
    </div>

module.exports = Docs  
