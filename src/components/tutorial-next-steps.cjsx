React = require 'react'
{ Link } = require 'react-router'
{ connect } = require 'react-redux'
{ updateTutorial } = require '../actions/tutorial'

TutorialNextSteps = React.createClass
  displayName: "Tutorial - Next steps"

  componentDidMount: ->
    { dispatch } = @props
    dispatch updateTutorial({step: null, completed: true})

  # coffeelint: disable=max_line_length
  render: ->
    <div className="page">
      <div className="tutorial">
        <div className="tutorial__intro-text">
          <h1>Next steps</h1>
          <p>
            For our simple lemonade stand, that&apos;s about it. You can use the graphs and logs on the Fuzzy.ai dashboard to monitor its performance and make sure it&apos;s always improving.
          </p>
          <h2>If you want to learn more</h2>
          <h3>here are some next steps:</h3>
          <ul>
            <li>
              Check out our <Link to="/docs">docs</Link> to find out more
              about creating, integrating, and optimizing fuzzy.ai agents.
            </li>
            <li>
              Try adding more inputs and rules to the lemonade stand agent. What if you include the number of people waiting in line? What if you include whether or not it&apos;s raining out?
            </li>
            <li>
              Try fuzzy.ai for your own application.
            </li>
          </ul>
        </div>

        <div className="pagination tuts-pagination">
          <Link to="/" className="btn btn-blue">Go to the dashboard and <strong>get started</strong></Link>
        </div>
      </div>

    </div>

mapStateToProps = (state) ->
  agent: state.agent.current
  tutorial: state.tutorial

module.exports = connect(mapStateToProps)(TutorialNextSteps)
