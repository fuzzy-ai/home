React = require 'react'
{ Link } = require 'react-router'

module.exports = React.createClass

  displayName: "Signup Complete"

  # coffeelint: disable=max_line_length
  render: ->
    <div className="row">
      <main className="Page_intro_zone main">
        <div className="unit size1of3 page_title_wrap ">
            <h1 className="page_title">Almost done! Confirm your email!</h1>
            <p>We&apos;ve sent a message to your email address to confirm
              your registration. Follow the instructions in that message to complete
              your registration.</p>
          </div>
        </main>
     </div>
