React = require 'react'
{ Link } = require 'react-router'

UpgradePrompt = React.createClass
  displayName: "Upgrade Prompt"

  # coffeelint: disable=max_line_length
  render: ->
    { month, plan } = @props

    if month >= plan?.apiLimit
      promptClass = "upgrade-prompt error-bar"
    else
      promptClass = "upgrade-prompt"

    <div className={promptClass}>
      {if month >= plan?.apiLimit
        <div className="upgrade-now-message">
          You have hit your API limit. Please <Link to="/pricing">Upgrade Now!</Link>
        </div>
      }
    </div>

module.exports = UpgradePrompt
