
React = require 'react'
{ Link } = require 'react-router'
{ connect } = require 'react-redux'

{ fetchAgents } = require '../actions/agent'


DashboardSidebar = React.createClass
  displayName: "Dashboard Sidebar"



  render:->
    { agents } = @props
    <div className="agent-sidebar">
      <Link to={@props.href} className="agent-sidebar__add-agent">Add an Agent
        <span className="square-plus">
          <svg className="svg-plus" xmlns="http://www.w3.org/2000/svg" id="Layer_1" data-name="Layer 1" viewBox="0 0 31.17 31.17">
            <path d="M15.56 0v31.17M0 15.56h31.17" className="cls-1"/>
          </svg>
        </span>
      </Link>
      <ul className="agent-sidebar__agent-list bordered-list ">
        { agents?.map (agent, i) ->
          <li className="bordered-list__item" key={i}><Link to="agents/#{agent.id}">{ agent.name }</Link></li>
        }
      </ul>
    </div>

mapStateToProps = (state) ->
  agents: state.agent.items

module.exports = connect(mapStateToProps)(DashboardSidebar)
