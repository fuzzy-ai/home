React = require 'react'
{ Link } = require 'react-router'

module.exports = React.createClass
  displayName: "Docs"

  # coffeelint: disable=max_line_length
  render: ->
    <div>
      <h1>Agent versions</h1>

      <pre>{'GET https://api.fuzzy.ai/agent/<agent id>/version'}</pre>

      <p>This method lists the previous versions of an agent.</p>

      <h2>Request</h2>

      <p>The request body is empty.</p>

      <h2>Response</h2>

      <p>The response is an UTF-8-encoded JSON array of strings. Each string is the version ID of a version of the Agent.</p>
      <p><a className="docs" id="getagentversion" name="getagentversion"></a></p>

      <pre>{'GET https://api.fuzzy.ai/agent/<agentid>/version/<version id>'}</pre>

      <p>This method gets a version of the Agent.</p>

      <h2>Request</h2>

      <p>The request body is empty.</p>

      <h2>Response</h2>

      <p>The response is an UTF-8-encoded JSON representation of the Agent version. It is identical to the Agent structure, with two differences:</p>

      <ul>
          <li>There is an additional &nbsp;<em>versionID</em> property.</li>
          <li>The &nbsp;<em>updatedAt</em> property is not included (since Agent versions are immutable).</li>
      </ul>
    </div>
