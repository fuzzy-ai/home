React = require 'react'
moment = require 'moment'
{ connect } = require 'react-redux'

{ fetchInvoice } = require '../actions/payments'

ProgressBar = require './progress-bar'
ErrorBar = require './error-bar'

LineItem = React.createClass
  displayName: "Line Item"

  render: ->
    { line } = @props

    if line.type == 'subscription'
      description = "Subscription to #{line.plan.name}"
      start = moment.unix(line.period.start)
      end = moment.unix(line.period.end)
      period = "#{start.format('YYYY-MM-DD')} - #{end.format('YYYY-MM-DD')}"
    else
      description = line.description
      period = ' N/A '
    <tr>
      <td>{description}</td>
      <td>{period}</td>
      <td>$ {line.amount / 100}</td>
    </tr>

InvoicesDetail = React.createClass
  displayName: "Invoice Detail"

  componentDidMount: ->
    { dispatch, params } = @props
    dispatch fetchInvoice(params.invoiceId)

  # coffeelint: disable=max_line_length
  render: ->
    { invoice, inProgress, error } = @props

    <div className="page">
      <div className="row">
        {if error?
          <ErrorBar err={error} />
        }
        <ProgressBar inProgress={inProgress} />
        <h1>Invoice: {invoice?.id}</h1>
        {if invoice?.date
          <h2>Date: {moment.unix(invoice.date).format('MMMM D, YYYY')}</h2>
        }
        {if invoice?.lines.data.length
          <table>
            <thead>
              <tr>
                <td>Description</td>
                <td>Period</td>
                <td>Amount</td>
              </tr>
            </thead>
            <tbody>
              {invoice.lines.data.map (line, i) ->
                <LineItem line={line} key={i} />
              }
              <tr>
                <td></td>
                <td><strong>Total:</strong></td>
                <td><strong>$ {invoice?.amount_due / 100}</strong></td>
              </tr>
            </tbody>
          </table>
        }
      </div>
    </div>

mapStateToProps = (state) ->
  invoice: state.payments.currentInvoice
  inProgress: state.payments.inProgress
  error: state.payments.error

module.exports = connect(mapStateToProps)(InvoicesDetail)
