React = require 'react'
{ Link } = require 'react-router'
{ connect } = require 'react-redux'

Landing = require './landing'
Dashboard = require './dashboard'

Home = React.createClass
  displayName: 'Home'

  render: ->
    <div>
      {if @props.user.authenticated
        <Dashboard />
      else
        <Landing />
      }
    </div>

mapStateToProps = (state) ->
  return {
    user: state.user
  }

module.exports = connect(mapStateToProps)(Home)
