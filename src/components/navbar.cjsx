React = require 'react'
{ Link } = require 'react-router'
{ connect } = require 'react-redux'
{ logOut } = require '../actions/user'

MainLoggedInNav = require './main-logged-in-nav'
MainNav = require './main-nav'
UserNav = require './user-nav'

Navbar = React.createClass
  displayName: "Navbar"

  logoutClick: (e) ->
    e.preventDefault()
    { dispatch } = @props
    dispatch(logOut())

  getInitialState: ->
    navOpen: false
    overlay: false
    burgerMenu: false
    settings: false

  toggleNav: ->
    @setState navOpen: false
    @setState overlay: true
    @setState burgerMenu: true

    @setState navOpen: not @state.navOpen
    @setState overlay: not @state.overlay
    @setState burgerMenu: not @state.burgerMenu

  toggleSettings: ->
    @setState settings: not @state.settings
    @setState settingsOverlay: not @state.settingsOverlay


  componentDidMount: ->
    masthead = document.querySelector('.masthead')
    masthead_height = getComputedStyle(masthead).height.split('px')[0]

    top = masthead?.offsetTop

    listener = ->
      y = window.pageYOffset
      if y >= 110
        masthead.classList.add 'masthead_lined'
      else
        masthead.classList.remove 'masthead_lined'

      return

    window.addEventListener 'scroll', listener, false


  # coffeelint: disable=max_line_length
  render: ->
    { authenticated, account } = @props.user

    { navOpen, overlay, burgerMenu, settings, settingsOverlay } = @state

    if navOpen
      MobiNavClass = "mobi_nav mobi_nav--is_active"
      overlayClass = "overlay--is_active"
      burgerMenuClass = "burger-lines actived-menu"
      handleNavToggle = @toggleNav
    else
      MobiNavClass = "mobi_nav  mobi_nav--is_closed"
      overlayClass = "overlay"
      burgerMenuClass = "burger-lines"
      handleNavToggle = @toggleNav

    if settings
      settingsClass = "settings__user-controls open_settings"
      settingsTriggerClass = "setting__trigger settings__trigger--closed"
      settingsOverlayClass = "setting__overlay "
      handleSettings = @toggleSettings
    else
      settingsClass = "settings__user-controls"
      settingsTriggerClass = "setting__trigger"
      settingsOverlayClass = "setting__overlay setting__overlay--closed"

      handleSettings = @toggleSettings

    <div className="masthead">
      <div className="logo-wrap flex">
        <Link to="/" className="logo">
          <img src="/assets/i/fuzzy-ai-logo.svg" id="logo_svg" />
        </Link>
      </div>
      <div className={MobiNavClass} onClick={handleNavToggle}>
        {if authenticated
          <MainLoggedInNav />
        else
          <MainNav />
          }

        {if authenticated
          <div className="settings__user-controls ">
            <span className="user-detail__email line">{ account.email }</span>
            <span className="user-detail__plan line">Current plan: <em>{ account.plan }</em></span>
            <Link to="/settings">settings</Link>
            <Link to="/logout" onClick={@logoutClick} className=" user-action-logout">Logout</Link>
          </div>
        else
          <UserNav />
        }
      </div>
      <div className={overlayClass}  onClick={handleNavToggle}></div>
      <div className={settingsOverlayClass}  onClick={handleSettings}></div>
      <div className={burgerMenuClass} onClick={handleNavToggle}><span></span></div>

      <div className="nav-mixed menu">
        <div className="main-nav">
          <nav id="multi-level-nav" className="multi-level-nav" role="navigation">
            {if authenticated
              <MainLoggedInNav />
            else
              <MainNav />
            }
          </nav>
        </div>
      </div>
      <div className="user-nav">
        <nav id="login_menu" className="login_menu" role="navigation">
          <div className="login_menu_inner">
            {if authenticated
              <div className="user" onClick={handleSettings}>
                <div className="user__logged-in">
                  <div className="user-detail">
                    <span className="user-detail__email line">{ account.email }</span>
                    <span className="user-detail__plan line">Current plan: <em>{ account.plan }</em></span>
                  </div>
                </div>
                <div className="settings">
                  <div className={settingsTriggerClass} onClick={handleSettings}>
                    <span className="settings__triangle"></span>
                    <span className="settings__triangle"></span>
                    <span className="settings__triangle"></span>
                  </div>
                </div>
                <div className={settingsClass} onClick={handleSettings}>
                  <span className="user-detail__email line">{ account.email }</span>
                  <span className="user-detail__plan line">Current plan: <em>{ account.plan }</em></span>
                  <Link to="/settings">settings</Link>
                  <Link to="/logout" onClick={@logoutClick} className=" user-action-logout">Logout</Link>
                </div>
              </div>
            else
              <UserNav />
            }
          </div>
         </nav>
       </div>
    </div>


mapStateToProps = (state) ->
  user: state.user

module.exports = connect(mapStateToProps)(Navbar)
