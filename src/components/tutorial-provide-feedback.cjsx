React = require 'react'
{ Link } = require 'react-router'
{ connect } = require 'react-redux'
{ updateTutorial } = require '../actions/tutorial'

TutorialProvideFeedback = React.createClass
  displayName: "Tutorial - Provide Feedback"

  componentDidMount: ->
    { dispatch } = @props
    dispatch updateTutorial({step: 'provide-feedback'})

  # coffeelint: disable=max_line_length
  render: ->
    <div className="page">
      <div className="tutorial">
        <h1>Provide Feedback</h1>
        <div className="tutorial__row">
          <div className="tutorial__text">
            <p>For many applications, the initial rules you set up for your agent will be sufficient. But for others, you&apos;ll want to optimize the output. To do this, you provide feedback based on the production results, which the agent will use to improve its outputs over time. </p>

            <p>For the lemonade stand, let&apos;s say that we measure performance based on how much revenue we get for each cup of lemonade. We&apos;ll call this &ldquo;revenue&rdquo;</p>
            <h2>Two things to note:</h2>
            <p>The agent needs to know which evaluation you are providing feedback on. To get this, you ask for metadata on the evaluation, and then provide the <code>reqID</code> metadata parameter.</p>
            <p>You call <code>feedback</code> to tell the agent how well it has done.</p>
            <p><Link to="/tutorial/next-steps" className="btn btn-blue">Find out about next steps</Link></p>
          </div>
          <div className="tutorial__image  f-right">
            <pre className="tut-box-pre pad-40 radius-4">
            {"""
              var FuzzyAIClient = require('fuzzy.ai');

              var API_KEY = 'your API key here';
              var AGENT_ID = 'your agent ID here';

              var client = new FuzzyAIClient(API_KEY);

              temperature = 85;

              client.evaluate(AGENT_ID, {temperature: temperature}, "meta", function(err, outputs)) {
                if (err) {
                  console.error(err);
                } else {
                  var revenue = outputs['price per cup'];
                  var revenue;

                  // Note: you have to let the customer decide whether they buy the product!

                  if (customerBuysProduct(results['price per cup'], temperature)) {
                    revenue = results['price per cup'];
                  } else {
                    revenue = 0;
                  }

                  client.feedback(outputs.meta.reqID, {"revenue": revenue}, function(err) {
                      if (err) {
                        consle.error(err);
                      }
                  });
                }
              });
            """}
            </pre>
            </div>


        </div>
      </div>
    </div>

mapStateToProps = (state) ->
  agent: state.agent.current
  tutorial: state.tutorial

module.exports = connect(mapStateToProps)(TutorialProvideFeedback)
