React = require 'react'
{ Link } = require 'react-router'

{ connect } = require 'react-redux'
{ push } = require 'react-router-redux'
{ newAgent } = require '../actions/agent'
{ updateTutorial } = require '../actions/tutorial'

SetOutput = require './agents-edit-set-output'
Template = require './new-agent'
Dashboard = require './dashboard'
DashboardSidebar = require './dashboard-sidebar'
Checklist = require './tutorial-checklist'


TutorialCreateAgent = React.createClass
  displayName: 'TutorialCreateAgent'

  getInitialState: ->
    agent:
      inputs: {}
      outputs: {}
      rules: []

  componentDidMount: ->
    { dispatch, tutorial, agent } = @props
    if tutorial?.agentID?
      dispatch push('/tutorial/add-a-first-rule')
    else
      data =
        step: 'create-an-agent'
      dispatch updateTutorial(data)

    tutToggle = document.querySelectorAll('.tut-toggle')
    tutExCont = document.querySelectorAll('.tut-extra-content')

    reset = ->
      tutExCont.forEach (content) ->
        if content.classList.contains('not-hidden')
          content.classList.remove 'not-hidden'
          content.classList.add 'hidden'
        return
      return

    openUp = (e) ->
      el = e?.currentTarget
      target = el?.nextElementSibling
      if target?.classList.contains('hidden')
        reset()
        el.classList.add 'tut-toggle-on'
        target.classList.remove 'hidden'
        target.classList.add 'not-hidden'
      else
        el.classList.remove 'tut-toggle-on'
        target.classList.add 'hidden'
        target.classList.remove 'not-hidden'
      return

    tutToggle.forEach (toggle) ->
      toggle?.addEventListener 'click', openUp, false
      return


  handleSubmit: (agent) ->
    { dispatch } = @props
    agent.name = _.keys(agent.outputs)[0]
    @setState agent: agent

    dispatch newAgent(agent, false)

  componentWillReceiveProps: (nextProps) ->
    { dispatch } = @props
    if nextProps.agent?.id != @props.agent?.id
      dispatch updateTutorial(agentID: nextProps.agent.id)
      .then ->
        dispatch push('/tutorial/add-a-first-rule')

  # coffeelint: disable=max_line_length
  render: ->
    {agent, mode} = @state
    { inProgress } = @props

    <div className="page">
      <div className="tutorial">
        <div className="tutorial__intro-text">
          <h1>Create your agent</h1>
          <p>To get started, create an agent. You need to name your agent and define its range of output values.</p>
        </div>
        <div className="tutorial__row">
          <div className="tutorial__text lined-steps">
            <p className="tut-toggle"><span className="num tut-step">01.</span> Name your agent: In the <strong>Name your agent</strong> field, enter &ldquo;Price per cup&rdquo;</p>
            <div className="tut-extra-content hidden">
            <p>An agent in Fuzzy.ai consists of a set of rules that produce a result. Your agent name should describe the problem you&apos;re trying to solve. For the lemonade stand, we want to know how much to charge for a cup of lemonade.
            </p>
            </div>

            <p className="tut-toggle"><span className="num tut-step">02.</span> Set your range: In the <strong>From</strong> field, enter 0.50 &amp; in the <strong>To</strong> field, enter 5</p>
            <div className="tut-extra-content hidden">

            <p>Here you set the range of values your agent will return. For this example we’re assuming that the minimum price you want to charge for a cup of lemonade is $0.50, and the maximum price is $5.
            </p>
            </div>
            <p><span className="num tut-step">03.</span> Click the <strong>Save</strong> button to go to the next step</p>
          </div>

          <figure className="tutorial__image">
            <SetOutput agent={agent} submitHandler={@handleSubmit} />
          </figure>
        </div>




      </div>
      <div className="pagination tuts-pagination">
        <Link to="/tutorial/add-a-first-rule" className="btn btn-blue">Go to step three: <strong>add a first rule</strong></Link>
        <br />
        <Link to="/tutorial" className="secondary-tut-link previous-page-link">Go to previous page</Link>
        <Link to="/agents/new" className="secondary-tut-link">Skip tutorial</Link>
      </div>
    </div>

mapStateToProps = (state) ->
  templates: state.agent.templates
  inProgress: state.agent.inProgress
  tutorial: state.tutorial
  agent: state.agent.current

module.exports = connect(mapStateToProps)(TutorialCreateAgent)
