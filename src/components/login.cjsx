React = require 'react'
{ Link } = require 'react-router'
{ connect } = require 'react-redux'
{ push } = require 'react-router-redux'

{ logIn, reset, resendConfirmation } = require '../actions/user'

ProgressBar = require './progress-bar'
ErrorBar = require './error-bar'

Login = React.createClass
  displayName: "Login"

  componentDidMount: ->
    { dispatch, user } = @props
    if user.authenticated
      dispatch push('/')
    else
      dispatch reset()

  componentWillReceiveProps: (nextProps) ->
    if nextProps.user.authenticated
      { dispatch } = @props
      dispatch(push('/'))

  handleSubmit: (e) ->
    e.preventDefault()
    email = @refs.email.value
    password = @refs.password.value

    {dispatch} = @props
    dispatch(logIn(
      email: email
      password: password
    ))

  handleReConfirm: (e) ->
    e.preventDefault()

    email = @refs.email.value

    {dispatch} = @props
    dispatch(resendConfirmation({email: email}))

  # coffeelint: disable=max_line_length
  render: ->
    { inProgress, authenticated, error, needsReConfirm } = @props.user

    <div className="row">
      <div className="page">
        {if error?
          <ErrorBar err={error} />
        }
        <ProgressBar inProgress={inProgress} />
        <p id="form-parent"></p>
        <div className="form-wrapper">
          <div className="form_text">
            <h1 className="page_title">Login</h1>
            <p className="mbl"><Link to="/request">I don&apos;t have an account yet.</Link></p>

          </div>
          <form action="/login" method="post" role="form" className="user-forms" onSubmit={@handleSubmit}>
            <div className="form-group input-wrap">
              <label htmlFor="email" className="upper">Email address</label>
              <input className="form-control" id="email" name="email" ref="email" placeholder="Enter email" type="email" />
            </div>
            <div className="form-group input-wrap">
              <label htmlFor="password" className="upper">Password</label>
              <input className="form-control" id="password" name="password" ref="password" placeholder="Password" type="password" />
              <a href="reset" className="form_user_request form_user_request__password hint">Forgot password?</a>
            </div>
            { if inProgress
              <button className="btn btn__regular" disabled="disabled">Working...</button>
            else
              <button className="btn btn__regular" type="submit">Login</button>
            }
          </form>
        </div>
      </div>
    </div>

mapStateToProps = (state) ->
  return {
    user: state.user
  }

module.exports = connect(mapStateToProps)(Login)
