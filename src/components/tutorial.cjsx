React = require 'react'
{ Link } = require 'react-router'

Checklist = require './tutorial-checklist'

module.exports = React.createClass
  displayName: "Tutorial"

  getInitialState: ->
    steps: []

  componentDidMount: ->
    steps = @props.route.childRoutes
    steps.unshift @props.route.indexRoute
    @setState
      steps: steps

  # coffeelint: disable=max_line_length
  render: ->
    current = @props.routes[@props.routes.length - 1]
    <div className="row">
      {@props.children}
      <Checklist steps={@state.steps} current={current} />
    </div>
