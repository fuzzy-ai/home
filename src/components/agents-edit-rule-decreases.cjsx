_ = require 'lodash'
React = require 'react'
Autocomplete = require './autocomplete'
RangeInput = require './range-input-popup'

AgentsEditRuleDecreases = React.createClass
  displayName: "AgentsEditRuleDecreases"

  getInitialState: ->
    error: ''
    editMode: false
    input: @props.rule?.input || ''
    showInputSettings: false
    inputSets: @props.agent?.inputs[@props.rule?.input] || [0, 100]


  componentDidMount: ->
    { rule, isNew, agent } = @props
    if isNew
      @setState editMode: true

    if not rule.output
      rule.output = _.keys(agent.outputs)[0]

  toggleEdit: (e) ->
    e.preventDefault()
    @setState editMode: not @state.editMode
    @props.toggleGlobalEdit?()

  handleSubmit: (e) ->
    e.preventDefault()

    { agent, rule, id, submitHandler } = @props
    rule.type = "decreases"
    rule.input = @state.input
    weight = @refs.weight?.value * 1

    if _.isNumber(weight) and weight >= 0.0 and weight <= 1.0
      rule.weight = weight

    # update Input Range
    if @state.inputSets
      agent.inputs[rule.input] = @state.inputSets

    other_rules = _.cloneDeep agent.parsed_rules
    delete other_rules[id]
    if not _.find other_rules, {type: rule.type, input: rule.input}
      submitHandler(agent, rule, id)
      @setState editMode: false
      @props.toggleGlobalEdit?()
    else
      @setState error: 'Duplicate rule: select a different input.'

  handleDelete: (e) ->
    e.preventDefault()
    { agent, id, deleteHandler } = @props
    if window.confirm("Are you sure?")
      deleteHandler(agent, id)

  handleInputChange: (value) ->
    { agent } = @props
    @setState input: value

  handleInputBlur: (value, options) ->
    if not options?.length
      @setState showInputSettings: true



  handleInputSets: (sets) ->
    @setState
      inputSets: sets
      showInputSettings: false


  handleRangeClose: ->
    if not @state.inputSets
      @handleInputSets([0, 100])
    @setState showInputSettings: false

  # coffeelint: disable=max_line_length
  render: ->
    { error, editMode, showInputSettings} = @state
    { rule, agent, isNew, inProgress } = @props

    if editMode
      ruleClass = "rule edit-mode"
      handleToggle = ->
    else
      handleToggle = @toggleEdit
      if @props.globalEdit
        ruleClass = "rule no-edit"
      else
        ruleClass = "rule"



    <div>

    <div className={ruleClass} onClick={handleToggle}>
      <svg viewBox="0 0 32 32" className="edit-hint ui-icon">
        <use xlinkHref="/assets/sprite.svg#icon-pencil"></use>
      </svg>
      <form onSubmit={@handleSubmit} className="rule-form">
        {if error
          <p className="error_message">{ error }</p>
        }
        <div className="rule-form__text-wrap">
          <div className="rule-form__text-wrap-inner">
            {if editMode
              <span className="rule-form__agent-input">
                <Autocomplete value={@state.input} items={_.keys(agent.inputs)} onChange={@handleInputChange} placeholder="name of input" onBlur={@handleInputBlur} />
              </span>
            else
              <span className="rule-form__input">{rule?.input} </span>
            }


            <span className="rule-form__function"> decreases </span>
            <span className="rule-form__constant">{rule?.output}. </span>
            {if editMode
              <RangeInput title={@state.input} input={@state.inputSets} saveHandler={@handleInputSets}  />
            }
            {if editMode
              <span className="rule-weight">
                <label htmlFor="weight">Set the Weight</label>
                <input type="number" min="0.0" max="1.0" step="0.01" className="rule-form__input" ref="weight" defaultValue={rule?.weight} />
              </span>
            else if rule?.weight
              <span className="rule-form__display-weight">Weight: {rule?.weight}</span>
            }
          </div>
        </div>
          {if not isNew
            <button className="rule-form-actions__cancel rule-btn " onClick={@toggleEdit}>
              <svg viewBox="0 0 32 32" className="cancel ui-icon">
                <use xlinkHref="/assets/sprite.svg#icon-cross"></use>
              </svg>
            </button>
          }
          {if editMode
            <div className="rule-form-actions ">
              { if inProgress
                <button type="submit" className="rule-form-actions__save rule-btn" disabled="disabled">Working...</button>
              else
                <button type="submit" className="rule-form-actions__save rule-btn">Save</button>
              }
              <button className="rule-form-actions__delete rule-btn" onClick={@handleDelete}>
                <svg className="delete-icon" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 32 32">
                  <use xlinkHref="/assets/sprite.svg#icon-trash"></use>
                </svg>
              </button>
            </div>
          else
            <div className="rule-form-actions action-hover">
              <button onClick={@toggleEdit} className="rule-form-actions__edit rule-btn ir">
                <svg className="pen" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 61.97 61.96">
                  <g className="pencil">
                    <path className="tip" className="cls-1" d="M0 61.96l16.16-5.39L5.39 45.8 0 61.96z"/>
                    <path className="eraser" className="cls-1" d="M41.76 9.43L51.184.004 61.96 10.78l-9.425 9.426z"/>
                    <path className="pen--body" className="cls-1" d="M7.304 44.36l32.23-32.23L49.85 22.444l-32.23 32.23z"/>
                  </g>
                </svg>
              </button>
            </div>
          }
      </form>
    </div>
    </div>

module.exports = AgentsEditRuleDecreases
