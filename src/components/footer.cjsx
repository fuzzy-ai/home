React = require 'react'
{ Link } = require 'react-router'
FuzzyVcard = require './fuzzy-vcard'
FuzzyFooterNav = require './fuzzy-footer-nav'
FuzzySocialFooterBar = require './fuzzy-social-footer-bar'
module.exports = React.createClass
  displayName: "Footer"

  # coffeelint: disable=max_line_length
  render: ->
    <footer className="footer">
      <div className="footer-inner">
        <div className="page">
          <FuzzyVcard />
          <FuzzyFooterNav />
          <div className="copyright">
            <p className="copyright__content upper">Copyright &copy; 2014&ndash;{(new Date().getFullYear())} fuzzy.ai. All rights reserved.</p>
          </div>
        </div>
      </div>
      <FuzzySocialFooterBar />
    </footer>
