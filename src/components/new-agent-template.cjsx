_ = require('lodash')

React = require 'react'
Link = require 'react-router/lib/Link'
{ push } = require 'react-router-redux'

{ connect } = require 'react-redux'
{ newAgent } = require '../actions/agent'
{ fetchTemplates } = require '../actions/templates'
ErrorBar = require './error-bar'

NewAgentTemplate = React.createClass
  displayName: 'New Agent from Template'

  componentDidMount: ->
    { dispatch } = @props
    dispatch(fetchTemplates())

  handleSubmit: (e) ->
    e.preventDefault()

    { dispatch, params, templates } = @props
    template = _.find templates, {slug: params.template}

    if template
      agent = template.source
      agent.name = template.name
      dispatch(newAgent(agent))

  # coffeelint: disable=max_line_length
  render: ->
    { templates, params, error } = @props
    template = _.find templates, {slug: params.template}

    <section className="new-agent-wrap">
      {if error?
        <ErrorBar err={error} />
      }
      {if template
        <main className="page-intro-zone">
          <header className="main-intro-text">
            <h1 className="page-title-wrap__heading inline big-title">New <em>{template.name}</em> agent.</h1>
            <p className="big-para">Create a new agent using the {template.name} template.</p>
            <p>{template.description}</p>
          </header>
          <form method="post" onSubmit={@handleSubmit}>
            <button type="input" className="btn btn__regular">Create</button>
          </form>
        </main>
      }
    </section>

mapStateToProps = (state) ->
  templates: state.templates.templates
  error: state.templates.error

module.exports = connect(mapStateToProps)(NewAgentTemplate)
