_ = require 'lodash'
React = require 'react'
{ Link } = require 'react-router'

module.exports = React.createClass
  displayName: 'PlanInfoSettings'

  handleSubmit: (e) ->
    e.preventDefault()

  # coffeelint: disable=max_line_length
  render: ->
    plan = _.find(@props.plans, {stripeId: @props.user?.plan})
    <div className="module Profile-module">
      <h2>Your Plan</h2>
      <ul className="user-profile compressed-list">
        <li className="user-profile__cur-plan compressed-list__item"> You are currently subscribed to: <b>{plan?.name}</b></li>
        <li className="user-profile__API-limit compressed-list__item">Limit of {plan?.apiLimit} API calls</li>
        <li className="user-profile__API-calls-made compressed-list__item"><span>{@props.stats?.total}</span> API calls made</li>
      </ul>
      <Link to="/pricing" className="btn">Upgrade plan</Link>
    </div>
