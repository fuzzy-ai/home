React = require 'react'
{ Link } = require 'react-router'

CompanyLogo = require ('./company-logo')

module.exports = React.createClass

  displayName: "home as seen on"

  # coffeelint: disable=max_line_length
  render: ->
    <div className="row svg-zone-wrapper">
      <div className="svg-zone page">
        <div className="svg-zone__heading-wrap">
          <h2>As seen on</h2>
        </div>
        <div className="svg-zone__inner">
          <CompanyLogo
            class= "newstack cie-logo"
            svgId= "#newsstack"
          />
          <CompanyLogo
            class= "oreilly cie-logo"
            svgId= "#oreilly"
          />
          <CompanyLogo
            class= "techcrunch cie-logo"
            svgId= "#techcrunch"
          />
          <CompanyLogo
            class= "harvard cie-logo"
            svgId= "#harvard-logo"
          />
          <CompanyLogo
            class= "ibm cie-logo"
            svgId= "#ibm"
          />
          <CompanyLogo
            class="sitepoint cie-logo"
            svgId= "#sitepoint"
          />

        </div>
      </div>
    </div>
