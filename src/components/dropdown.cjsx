React = require 'react'
{ PlusIcon } = require './icons'

Dropdown = React.createClass
  displayName: 'dropdown'

  getInitialState: ->
    showDropdown: false


  toggleDropdown: (e) ->
    e.preventDefault()
    @setState showDropdown: not @state.showDropdown

  handleClick: (e) ->
    e.preventDefault()
    value = e.target.getAttribute('data-value')
    @props.selectHandler?(value)
    @setState showDropdown: false

  # coffeelint: disable=max_line_length
  render: ->
    { title, options } = @props
    { showDropdown } = @state
    { handleClick } = this

    if showDropdown
      dropdownButtonClass = "dropdown-title dropdown-title-inline dropdown-title--is-on"
      handleToggle = @toggleDropdown
    else
      dropdownButtonClass = "dropdown-title dropdown-title-inline"
      handleToggle = @toggleDropdown

    <span className="dropdown dropdown-inline-rules">
      <span className={dropdownButtonClass} onClick={handleToggle}>{title}
        <span className="dropdown__plus-icon">
          <PlusIcon />
        </span>
      </span>
      {if @state.showDropdown
        <ul className="dropdown-options dropdown-inline-rules__options">
          {options.map (option, i) ->
            <li key={i}><a data-value={option.value} onClick={handleClick}>{option.label}</a></li>
          }
        </ul>
      }
    </span>

module.exports = Dropdown
