React = require 'react'
_ = require 'lodash'
d3 = require 'd3-format'

{ Link } = require 'react-router'
{ connect } = require 'react-redux'

{sigfigs, fmt} = require '../sigfigs'

class TestResults extends React.Component

  displayName: 'Test Results'

  # coffeelint: disable=max_line_length
  render: ->
    { userID, agentID, versionID, inputs, outputs, fuzzified, rules, inferred, clipped, combined, centroid, createdAt, timing } = @props

    sig = sigfigs inputs

    <div className="test-results" data-user-id={userID} data-agent-id={agentID} data-version-id={versionID}>
      <div className="calculated-test-results-outputs">
        <ul className="test-results-output-list">
        { _.map _.omit(outputs, "_evaluation_id"), (value, name) ->
          <li key={"test-results-output-item-" + _.kebabCase(name)} id={"test-results-output-item" + _.kebabCase(name)} className="test-results-output-list__item">
            <span key={"test-results-output-name-" + _.kebabCase(name)} className="test-results-output-list__name h4 upper fwb agent-test-name">{name}:</span>
            <span key={"test-results-output-value-" + _.kebabCase(name)} className="test-results-output-list__value spaced-loose num fwn value">{fmt(value, sig)}</span>
          </li>
        }
        </ul>
        <small>Calculated in {Math.round(timing)} milliseconds.</small>
      </div>
    </div>

module.exports = TestResults
