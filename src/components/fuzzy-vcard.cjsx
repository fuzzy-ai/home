React = require 'react'
{ Link } = require 'react-router'

module.exports = React.createClass

  displayName: "fuzzy vcard"

  render:->
    <div className="vcard">
      <strong>Fuzzy.ai</strong><br />
      <span className="street-address">5333 Avenue Casgrain suite 1227</span>
      <span className="region line">Montreal, QC. </span>
      <span className="postal-code">H2T 1X3</span>
      <span className="country-name"> Canada</span>
      <br /><a href="mailto:support@fuzzy.ai">support@fuzzy.ai</a>
      <span className="tel line"><a href="tel:888-602-9675">888-602-9675</a></span>
    </div>
