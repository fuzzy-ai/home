_ = require 'lodash'
React = require 'react'
CSSTransitionGroup = require 'react-addons-css-transition-group'

{ Link } = require 'react-router'
{ connect } = require 'react-redux'
{ fetchAgent } = require '../actions/agent'
{ editRule, deleteRule } = require '../actions/agent'

{ GearIcon, EditIcon, BeakerIcon, StatsIcon } = require './icons'

AgentsNav = React.createClass
  displayName: "Agents Nav"

  componentDidMount: ->

    navItem = document.querySelectorAll('.setting-icons')
    navEditItem = document.querySelector('#edit-nav')
    i = 0
    while i < navItem.length
      navItem[i]?.addEventListener 'click', ->
        if navEditItem.classList.contains('active')
          navEditItem.classList.toggle('active')
        else
          navItem[i]?.classList.toggle 'active'
        return
      i++

    navEditItem.addEventListener 'click', ->
      navEditItem.classList.toggle('active')

  # coffeelint: disable=max_line_length
  render: ->
    { agent } = @props
    {location} = @props.location.pathname

    <CSSTransitionGroup
      component="div"
      className="agent-nav"
      transitionEnterTimeout={800}
      transitionLeaveTimeout={800}
      transitionName="agent-nav"
      key="agent-nav"
    >

      <Link to="/agents/#{agent?.id}/settings"  className="agent__setting setting-icons" activeClassName="active" title="settings">
        <div className="nav-ui-icon-wrapper">
          <GearIcon />
        </div>
        <span className="agent-nav__text ">Settings</span>
      </Link>
      <Link to="/agents/#{agent?.id}/stats" className="agent-stats setting-icons" activeClassName="active" title="view agent's stats">
        <div className="nav-ui-icon-wrapper">
          <StatsIcon />
        </div>
        <span className="agent-nav__text ">Stats</span>
      </Link>
      <Link to="/agents/#{agent?.id}" id="edit-nav" className="agents-edit setting-icons" activeClassName="active" title="agent">
        <div className="nav-ui-icon-wrapper">
          <EditIcon />
        </div>
        <span className="agent-nav__text ">Edit</span>
      </Link>
      <Link to="/agents/#{agent?.id}/test" className="agents-test setting-icons" activeClassName="active" title="test your agent">
        <div className="nav-ui-icon-wrapper">
          <BeakerIcon />
        </div>
        <span className="agent-nav__text ">Test</span>
      </Link>
    </CSSTransitionGroup>



mapStateToProps = (state) ->
  agent: state.agent.current

module.exports = connect(mapStateToProps)(AgentsNav)
