React = require 'react'
{ Link } = require 'react-router'
{ PlusIcon } = require './icons'

module.exports = React.createClass
  displayName: "Docs Menu"


  componentDidMount: ->
    docTrig = document.querySelector '.docs-nav-trigger'
    docSidebar = document.querySelector '.docs-sidebar'


    #toggle menu with button
    toggleDocsMenu = ->
      docSidebar.classList.toggle 'expand-is-open'
      return
    #add event listener to button
    docTrig?.addEventListener 'click', toggleDocsMenu

    #toggle menu when user clicks on a docs link container das cheats yo

    linkToggleDocsMenu = ->
      docSidebar.classList.toggle 'expand-is-open'
      return

    #add event listerner to container
    docSidebar?.addEventListener 'click', linkToggleDocsMenu



  # coffeelint: disable=max_line_length
  render: ->
    <div>
    <a href="#" className="docs-nav-trigger">Docs Navigation <span className="trigger-sub-docs"><PlusIcon /></span></a>
      <ul className="docs-sidebar compressed-list expand">
        <li className="compressed-list__item docs-sidebar__list">
          <Link  to="/docs/getting-started" activeClassName="active-doc-nav" className="docs-sidebar__list-anchor fwb">Getting Started</Link> </li>
        <li className="compressed-list__item docs-sidebar__list">
          <Link  to="/docs/sdk" className="docs-sidebar__list-anchor fwb" activeClassName="active-doc-nav">SDKs </Link>
          <ul className="docs-sidebar__ul">
            <li className="compressed-list__item docs-sidebar__list">
              <Link  to="/docs/sdk/nodejs" className="docs-sidebar__list-anchor" activeClassName="active-doc-nav" >NodeJS</Link> </li>
            <li className="compressed-list__item docs-sidebar__list">
              <Link  to="/docs/sdk/php" className="docs-sidebar__list-anchor" activeClassName="active-doc-nav">PHP</Link> </li>
            <li className="compressed-list__item docs-sidebar__list">
              <Link  to="/docs/sdk/python" className="docs-sidebar__list-anchor" activeClassName="active-doc-nav">Python</Link> </li>
            <li className="compressed-list__item docs-sidebar__list">
              <Link  to="/docs/sdk/ruby" className="docs-sidebar__list-anchor" activeClassName="active-doc-nav" >Ruby</Link> </li>
          </ul>
        </li>
        <li className="compressed-list__item docs-sidebar__list" >
          <Link  to="/docs/rest" className="docs-sidebar__list-anchor fwb" activeClassName="active-doc-nav">REST API </Link>
          <ul className="docs-sidebar__ul">
            <li className="compressed-list__item docs-sidebar__list">
              <Link  to="/docs/rest/about-rest" className="docs-sidebar__list-anchor" activeClassName="active-doc-nav">About REST</Link> </li>
            <li className="compressed-list__item docs-sidebar__list">
              <Link  to="/docs/rest/data" className="docs-sidebar__list-anchor" activeClassName="active-doc-nav" >Data</Link> </li>
            <li className="compressed-list__item docs-sidebar__list">
              <Link  to="/docs/rest/authentication" className="docs-sidebar__list-anchor" activeClassName="active-doc-nav" >Authentication</Link> </li>
            <li className="compressed-list__item docs-sidebar__list">
              <Link  to="/docs/rest/inference" className="docs-sidebar__list-anchor" activeClassName="active-doc-nav">Inference</Link> </li>
            <li className="compressed-list__item docs-sidebar__list">
              <Link  to="/docs/rest/agent" className="docs-sidebar__list-anchor" activeClassName="active-doc-nav">Agent lifecycle</Link> </li>
            <li className="compressed-list__item docs-sidebar__list">
              <Link  to="/docs/rest/agent-version" className="docs-sidebar__list-anchor" activeClassName="active-doc-nav">Agent version</Link> </li>
            <li className="compressed-list__item docs-sidebar__list">
              <Link  to="/docs/rest/evaluation" className="docs-sidebar__list-anchor" activeClassName="active-doc-nav" >Evaluation and feedback</Link> </li>
            <li className="compressed-list__item docs-sidebar__list">
              <Link  to="/docs/rest/api-version" className="docs-sidebar__list-anchor" activeClassName="active-doc-nav">API version</Link> </li>
          </ul>
        </li>
      </ul>
    </div>
