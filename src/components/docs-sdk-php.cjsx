React = require 'react'
{ Link } = require 'react-router'

module.exports = React.createClass
  displayName: "Docs"

  # coffeelint: disable=max_line_length
  render: ->
    <div>
      <h1>PHP</h1>
      <p>You can use fuzzy.ai from your PHP project using the Open Source library.</p>
      <section>
        <h2>Requirements</h2>
        <p>PHP 5.3.3 or later with the cURL extension.</p>
      </section>
      <section>
        <h2>Installation</h2>
        <p>You can install the library via <a href="http://getcomposer.org/">Composer</a>:</p>
        <pre>
              composer require fuzzy-ai/sdk
        </pre>
        <p>To load the library, use Composer's autoload:</p>
        <pre>
            require_once('vendor/autoload.php');
        </pre>
      </section>
      <section>
        <h2>Usage</h2>
        <p>All API calls require an API key from <a href="https://fuzzy.ai/">https://fuzzy.ai/</a>.</p>
        <pre>
            $client = new FuzzyAi\Client('YourAPIKey');

            list($result, $evalID) = $client->evaluate('YourAgentID', array('input1' => 42));
        </pre>
      </section>
      <section>
        <h2>Examples</h2>
        <p>The <code>examples</code> directory has some examples of using the library.</p>
      </section>
    </div>
