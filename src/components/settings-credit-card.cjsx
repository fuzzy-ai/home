React = require 'react'
{ Link } = require 'react-router'

CreditCard = require './credit-card'

module.exports = React.createClass
  displayName: 'CreditCardSettings'

  getInitialState: ->
    showCardForm: false

  handleClick: (e) ->
    e.preventDefault()
    @setState {showCardForm: not @state.showCardForm}

  # coffeelint: disable=max_line_length
  render: ->
    { error, submitHandler } = @props
    card = @props.customer?.sources?.data[0]
    <div className="module credit-info">
      <h2>Credit Card Information</h2>
      {if card
        <div>
          <p>Your credit card ends with <span className="credit-info__card-num-hint num">{card?.last4}</span> </p>
          <div><a onClick={@handleClick} className="fwb upper small-text">Update your credit card Information</a></div>
          { if @state.showCardForm
            <CreditCard submitHandler={submitHandler}/>
          }
        </div>
      else
        <div>
          <p>No credit card on file.</p>
          <Link to="/pricing" className="btn">Upgrade plan</Link>
        </div>
      }
    </div>
