React = require 'react'

FuzzyExpression = React.createClass

  # coffeelint: disable=max_line_length
  render: ->
    { json } = @props

    elementType = switch json?.type
      when 'and'
        AndExpression
      when 'or'
        OrExpression
      when 'not'
        NotExpression
      else
        IsExpression

    React.createElement elementType, @props, null

module.exports = FuzzyExpression

# Prevent circular imports causing issues with webpack
# http://stackoverflow.com/a/30390378
IsExpression = require './fuzzy-expression-is'
AndExpression = require './fuzzy-expression-and'
OrExpression = require './fuzzy-expression-or'
NotExpression = require './fuzzy-expression-not'
