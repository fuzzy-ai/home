React = require 'react'
{ Link } = require 'react-router'

module.exports = React.createClass
  displayName: "Docs"

  # coffeelint: disable=max_line_length
  render: ->
    <div>
      <h1>Authentication</h1>

      <p>The fuzzy.ai API uses &nbsp;<a href="https://tools.ietf.org/html/rfc6750">OAuth 2.0 Bearer Tokens</a> for authentication of your requests. This requires using your API key, available from the account page or your dashboard.</p>
      <p>Most fuzzy.ai API endpoints require authentication. To indicate your identity, add an Authorization header to your request indicating the Bearer authorization scheme and your API key, like this:</p>

      <pre>
        Authorization: Bearer&nbsp;<em>{'<your API key here>'}</em>
      </pre>

      <p><strong>Note: Your API key is very sensitive! If someone else obtains your API key, they can use it to use, change or delete your fuzzy agents.</strong> Treat your API key like any other sensitive data.</p>
    </div>
