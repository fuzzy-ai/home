React = require 'react'
_ = require 'lodash'

{ connect } = require 'react-redux'

Log = require './log'

LogList = React.createClass

  displayName: 'Log List'

  render: ->
    { logs, agents } = @props
    agentMap = {}
    if agents?
      for agent in agents
        agentMap[agent.id] = agent

    <table className='log-list responsive-table' data-logs-count={logs?.length}>
      <tbody>
        <tr>
          <th className="upper fwb">
            Time
          </th>
          <th className="upper fwb">
            Agent
          </th>
          <th className="upper fwb">
            Inputs
          </th>
          <th className="upper fwb">
            Outputs
          </th>
        </tr>
        { _.map logs, (log, i) ->
          agent = agentMap[log.agentID]
          <Log key={log.reqID} agent={agent} {...log} />
        }
      </tbody>
    </table>

module.exports = LogList
