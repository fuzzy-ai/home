React = require 'react'
{ PropTypes } = require 'react'
Link = require 'react-router/lib/Link'

class Plan extends React.Component
  displayName: "Plan"

  # coffeelint: disable=max_line_length
  render: ->
    {plan, current, authenticated} = @props
    planClasses = "pricing-package  flex-grid-col"
    if current?.stripeId == plan.stripeId
      planClasses += " current"

    numberWithCommas = (num) ->
      num.toString().replace /\B(?=(\d{3})+(?!\d))/g, ','


    <div className={planClasses}>
      <div className="plan-header">
        <h3 className="plan-header__name spaced-normal fwh upper">{plan.name}</h3>
        <div className="plan-price">
          <span className="plan-price__num"><span className="plan-price__dollar-sign">$</span>{plan.price / 100}</span>
          <span className="plan-price__month"> /{plan.period}</span>
        </div>
      </div>
      <ul className="plan plan1 compressed-list">
        <li className="compressed-list__item"><strong>{numberWithCommas(plan.apiLimit)} </strong>Monthly API calls</li>
        <li className="compressed-list__item"><strong>Chat & email </strong>support</li>
        <li className="compressed-list__item">&mdash;</li>
        <li className="compressed-list__item">&mdash;</li>
      </ul>
      <div className="price-btn-wrapper">
        {if not authenticated
          <Link className="btn btn__regular" to="/signup">Get Started</Link>
        else if current?.stripeId == plan.stripeId
          <span className="btn btn__regular">Current Plan</span>
        else if plan.price < current?.price
          <Link className="btn btn__regular" to="/upgrade?plan=#{plan.stripeId}">Downgrade</Link>
        else
          <Link className="btn btn__regular" to="/upgrade?plan=#{plan.stripeId}">Upgrade</Link>
        }
      </div>
    </div>

Plan.propTypes =
  plan: React.PropTypes.object
  current: React.PropTypes.object

module.exports = Plan
