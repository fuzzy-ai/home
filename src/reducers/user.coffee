# src/reducers/user.coffee
# Copyright 2016 9165584 Canada Corporation <legal@fuzzy.ai>
# All rights reserved.

_ = require 'lodash'

initialState =
  inProgress: false
  authenticated: false
  account: null
  error: null
  needsReConfirm: false

module.exports = (state = initialState, action) ->
  newState = switch (action?.type)
    when 'USER_RESET'
      initialState
    when 'USER_SET'
      account: _.assign {}, state.account, action.user
    when 'LOGIN_USER'
      inProgress: true
    when 'LOGIN_USER_SUCCESS'
      inProgress: false
      authenticated: true
      error: null
      account: action.user
    when 'LOGIN_USER_ERROR'
      inProgress: false
      authenticated: false
      error: action.error
      needsReConfirm: action.needsReConfirm
    when 'LOGOUT_USER'
      inProgress: true
    when 'LOGOUT_USER_SUCCESS'
      initialState
    when 'LOGOUT_USER_ERROR'
      inProgress: false
      authenticated: true
    when 'REQUEST_INVITE'
      inProgress: true
    when 'REQUEST_INVITE_SUCCESS'
      inProgress: false
      error: null
    when 'REQUEST_INVITE_ERROR'
      inProgress: false
      error: action.message
    when 'SIGNUP'
      inProgress: true
    when 'SIGNUP_SUCCESS'
      inProgress: false
      error: null
    when 'SIGNUP_ERROR'
      inProgress: false
      needsReConfirm: action.needsReConfirm
      error: action.message
    when 'RESEND_CONFIRMATION'
      inProgress: true
    when 'RESEND_CONFIRMATION_SUCCESS'
      inProgress: false
      needsReConfirm: false
      error: action.message
    when 'RESEND_CONFIRMATION_ERROR'
      inProgress: false
      needsReConfirm: false
      error: action.message
    when 'UPDATE_USER'
      inProgress: true
    when 'UPDATE_USER_SUCCESS'
      inProgress: false
      authenticated: true
      error: null
      account: action.user
    when 'UPDATE_USER_ERROR'
      inProgress: false
      error: action.error
    when 'CHANGE_PASSWORD'
      inProgress: true
    when 'CHANGE_PASSWORD_SUCCESS'
      inProgress: false
      error: null
    when 'CHANGE_PASSWORD_ERROR'
      inProgress: false
      error: action.error
    when 'RESET_REQUEST'
      inProgress: true
    when 'RESET_REQUEST_SUCCESS'
      inProgress: false
    when 'RESET_REQUEST_ERROR'
      inProgress: false
      error: action.error
    when 'RESET_PASSWORD'
      inProgress: true
    when 'RESET_PASSWORD_SUCCESS'
      inProgress: false
      error: null
    when 'RESET_PASSWORD_ERROR'
      inProgress: false
      error: action.error

  if newState?
    _.assign {}, state, newState
  else
    state
