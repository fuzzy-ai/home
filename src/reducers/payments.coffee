# src/reducers/payments.coffee
# Copyright 2016 9165584 Canada Corporation <legal@fuzzy.ai>
# All rights reserved.

_ = require 'lodash'

initialState =
  inProgress: false
  error: null
  customer: {}
  invoices: []
  currentInvoice: null
  data: null

module.exports = (state = initialState, action) ->
  newState = switch (action?.type)
    when 'CREATE_CUSTOMER'
      inProgress: true
      error: null
    when 'CREATE_CUSTOMER_ERROR'
      inProgress: false
      error: action.message
    when 'GET_CUSTOMER'
      inProgress: true
    when 'GET_CUSTOMER_SUCCESS'
      inProgress: false
      customer: action.customer
    when 'GET_CUSTOMER_ERROR'
      inProgress: false
      error: action.error
      customer: {}
    when 'UPDATE_CUSTOMER'
      inProgress: true
      error: null
    when 'UPDATE_CUSTOMER_SUCCESS'
      inProgress: false
      customer: action.customer
    when 'UPDATE_CUSTOMER_ERROR'
      inProgress: false
      error: action.error
      data: action.data
    when 'GET_INVOICES'
      inProgress: true
    when 'GET_INVOICES_SUCCESS'
      inProgress: false
      invoices: action.invoices
    when 'GET_INVOICES_ERROR'
      inProgress: false
      error: action.error
    when 'GET_INVOICE'
      inProgress: true
    when 'GET_INVOICE_SUCCESS'
      inProgress: false
      currentInvoice: action.currentInvoice
    when 'GET_INVOICE_ERROR'
      inProgress: false
      error: action.error

  if newState?
    _.assign {}, state, newState
  else
    state
