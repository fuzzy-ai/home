# home

This is the main landing page and IDE for fuzzy.ai

# LICENSE

    fuzzy.ai home
    Copyright (C) 2014-2018 Fuzzy.ai

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.

Images and documentation that Fuzzy.ai made are licensed under the Creative
Commons ShareAlike 3.0 license (CC-BY-SA 3.0).

# Configuration variables

This code accepts the following environment variables for configuration.

* WEB_PORT: Port to listen on, default = 80
* WEB_HOSTNAME: Hostname to use for URLs; default is 'localhost'
* WEB_ADDRESS: Address to listen on; default is '0.0.0.0'
* WEB_KEY: SSL key (not the name of the file; the full key!). Default is null.
* WEB_CERT: SSL cert; default = null
* WEB_AUTH_SERVER: root URL of auth microservice
* WEB_AUTH_KEY: OAuth token to use for auth microservice
* WEB_SESSION_SECRET: secret to use for storing web sessions. Default is
  garbage.
* WEB_API_SERVER: root URL of API server, preferably on the internal interface
* WEB_API_KEY: OAuth token to use for API server
* WEB_LOG_LEVEL: Log level to use. Default is "info".
* WEB_STATS_SERVER: root URL of the stats microservice
* WEB_STATS_KEY: OAuth token for the stats microservice
* WEB_PLAN_SERVER: root URL of the plan microservice
* WEB_PLAN_KEY: OAuth token for the plan microservice
* WEB_PAYMENTS_SERVER: root URL of the payments microservice
* WEB_PAYMENTS_KEY: OAuth token for the payments microservice
* WEB_TEMPLATES_SERVER: root URL of the templates microservice
* WEB_TEMPLATES_KEY: OAuth token for the templates microservice
* WEB_CA_CERT: A full cert for a private CA. We don't use this anymore.
* DRIVER: Databank driver to use for session storage.
* PARAMS: Databank driver params for session storage.
* CLEANUP: Session cleanup frequency in microseconds. Default is 1 hour.
* SLACK_HOOK: Hook to post error info to.
* API_CLIENT_CACHE_SIZE: How big the client cache should be. Default 50.
* WEB_CLIENT_TIMEOUT: How long to wait when making WebClient requests.
* REDIRECT_TO_HTTPS: Whether to redirect from http: to https:. The Kubernetes
  load balancer can't handle this so we have to do it ourselves. Default false.
* WEB_SESSION_MAX_AGE: Max age for the session. Default is 30 days.
* WEB_SESSION_SECURE: Whether the web session cookie should be secure (only
  https). Default is false.
