# integration-login-test.coffee
# Copyright 2016 9165584 Canada Corporation <legal@fuzzy.ai>
# All rights reserved.

vows = require 'perjury'
assert = vows.assert

{ browser, loginBatch } = require './apibatch'

vows
  .describe("GET /upgrade")
  .addBatch loginBatch
    'and we visit /upgrade':
      topic: ->
        browser.visit '/upgrade?plan=standard', @callback
        undefined
      'it works': ->
        browser.assert.success()
      'it has a name input': ->
        browser.assert.input('#name', '')
      'it has a zip code input': ->
        browser.assert.input('#address_zip', '')
      'it has a number input': ->
        browser.assert.input('#number', '')
      'it has a CVC input': ->
        browser.assert.input('#cvc', '')
      'it has an expiry date input': ->
        browser.assert.input('#exp-month', '')
        browser.assert.input('#exp-year', '')
  .export(module)
