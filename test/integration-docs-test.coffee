# integration-docs-test.coffee
# Copyright 2016 9165584 Canada Corporation <legal@fuzzy.ai>
# All rights reserved.

vows = require 'perjury'
assert = vows.assert

{ browser, apiBatch } = require './apibatch'

vows
  .describe("GET /docs/*")
  .addBatch apiBatch
    'and we visit /docs':
      topic: ->
        browser.visit '/docs', @callback
        undefined
      'it works': ->
        browser.assert.success()
      'and we visit /docs/getting-started':
        topic: ->
          browser.visit '/docs/getting-started', @callback
          undefined
        'it works': ->
          browser.assert.success()
      'and we visit /docs/sdk/nodejs':
        topic: ->
          browser.visit '/docs/sdk/nodejs', @callback
          undefined
        'it works': ->
          browser.assert.success()
      'and we visit /docs/sdk/python':
        topic: ->
          browser.visit '/docs/sdk/python', @callback
          undefined
        'it works': ->
          browser.assert.success()
      'and we visit /docs/sdk/ruby':
        topic: ->
          browser.visit '/docs/sdk/ruby', @callback
          undefined
        'it works': ->
          browser.assert.success()
      'and we visit /docs/sdk/php':
        topic: ->
          browser.visit '/docs/sdk/php', @callback
          undefined
        'it works': ->
          browser.assert.success()
      'and we visit /docs/rest':
        topic: ->
          browser.visit '/docs/rest', @callback
          undefined
        'it works': ->
          browser.assert.success()
        'and we visit /docs/rest/about-rest':
          topic: ->
            browser.visit '/docs/rest/about-rest', @callback
            undefined
          'it works': ->
            browser.assert.success()
        'and we visit /docs/rest/data':
          topic: ->
            browser.visit '/docs/rest/data', @callback
            undefined
          'it works': ->
            browser.assert.success()
        'and we visit /docs/rest/authentication':
          topic: ->
            browser.visit '/docs/rest/authentication', @callback
            undefined
          'it works': ->
            browser.assert.success()
        'and we visit /docs/rest/inference':
          topic: ->
            browser.visit '/docs/rest/inference', @callback
            undefined
          'it works': ->
            browser.assert.success()
        'and we visit /docs/rest/agent':
          topic: ->
            browser.visit '/docs/rest/agent', @callback
            undefined
          'it works': ->
            browser.assert.success()
        'and we visit /docs/rest/agent-version':
          topic: ->
            browser.visit '/docs/rest/agent-version', @callback
            undefined
          'it works': ->
            browser.assert.success()
        'and we visit /docs/rest/evaluation':
          topic: ->
            browser.visit '/docs/rest/evaluation', @callback
            undefined
          'it works': ->
            browser.assert.success()
        'and we visit /docs/rest/api-version':
          topic: ->
            browser.visit '/docs/rest/api-version', @callback
            undefined
          'it works': ->
            browser.assert.success()

  .export(module)
