# integration-login-test.coffee
# Copyright 2016 9165584 Canada Corporation <legal@fuzzy.ai>
# All rights reserved.

vows = require 'perjury'
assert = vows.assert

{ browser, loginBatch } = require './apibatch'

vows
  .describe("GET /upgraded")
  .addBatch loginBatch
    'and we visit /upgrade':
      topic: ->
        browser.visit '/upgraded', @callback
        undefined
      'it works': ->
        browser.assert.success()
  .export(module)
