# integration-signup-complete-test.coffee
# Copyright 2016 9165584 Canada Corporation <legal@fuzzy.ai>
# All rights reserved.

vows = require 'perjury'
assert = vows.assert

{ browser, apiBatch } = require './apibatch'

vows
  .describe("GET /signup-complete")
  .addBatch apiBatch
    'and we visit /signup-complete':
      topic: ->
        browser.visit '/signup-complete', @callback
        undefined
      'it works': ->
        browser.assert.success()
  .export(module)
