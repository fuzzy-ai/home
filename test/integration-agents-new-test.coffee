# integration-agents-new-test.coffee
# Copyright 2016 9165584 Canada Corporation <legal@fuzzy.ai>
# All rights reserved.

vows = require 'perjury'
assert = vows.assert

{ browser, loginBatch } = require './apibatch'

vows
  .describe("GET /agents/new")
  .addBatch loginBatch
    'and we visit /agents/new':
      topic: ->
        browser.visit '/agents/new', @callback
        undefined
      'it works': ->
        browser.assert.success()
      'it has at least two templates': ->
        browser.assert.elements('.new-agent__card', {atLeast: 2})
      'it has an empty template': ->
        browser.assert.text('.new-agent__card a', /Empty/)
  .export(module)
