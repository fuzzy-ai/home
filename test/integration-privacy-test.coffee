# integration-privacy-test.coffee
# Copyright 2016 9165584 Canada Corporation <legal@fuzzy.ai>
# All rights reserved.

vows = require 'perjury'
assert = vows.assert

{ browser, apiBatch } = require './apibatch'

vows
  .describe("GET /privacy")
  .addBatch apiBatch
    'and we visit /privacy':
      topic: ->
        browser.visit '/privacy', @callback
        undefined
      'it works': ->
        browser.assert.success()
      'it has a Privacy Policy header': ->
        browser.assert.text('h2', 'Privacy Policy')
  .export(module)
