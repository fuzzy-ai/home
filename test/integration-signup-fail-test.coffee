# integration-login-test.coffee
# Copyright 2016 9165584 Canada Corporation <legal@fuzzy.ai>
# All rights reserved.

vows = require 'perjury'
assert = vows.assert

{ browser, apiBatch } = require './apibatch'

vows
  .describe("Failed /signup")
  .addBatch apiBatch
    'and we visit /signup':
      topic: ->
        browser.visit '/signup', @callback
        undefined
      'it works': ->
        browser.assert.success()
      'and we submit with a missing field':
        topic: ->
          browser
            .fill 'password', 'unit-test-password'
            .fill 'confirm', 'unit-test-password'
            .pressButton 'Signup', @callback
          undefined
        'it works': ->
          browser.assert.success()
        'we see errors': ->
          browser.assert.elements('.error', {atLeast: 2})
        'and we submit mismatched passwords':
          topic: ->
            browser
              .fill 'email', 'unittest@example.com'
              .fill 'password', 'first-password'
              .fill 'confirm', 'second-password'
              .check '#tos'
              .pressButton 'Signup', @callback
            undefined
          'it works': ->
            browser.assert.success()
          'we see errors': ->
            browser.assert.elements('.error')
          'and we submit an invalid coupon code':
            topic: ->
              browser.fill 'email', 'unittest@example.com'
                .fill 'password', 'unit-test-password'
                .fill 'confirm', 'unit-test-password'
                .fill 'code', 'INVALID_CODE'
                .check '#tos'
                .pressButton 'Signup', @callback
              undefined
            'it works': ->
              browser.assert.success()
            'we see errors': ->
              browser.assert.elements('.error')
  .export(module)
